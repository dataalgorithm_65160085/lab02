public class RemoveElement {
    public static void main(String[] args) {
        int nums1[] = { 3, 2, 2, 3 };
        int val1 = 3;
        int nums2[] = { 0, 1, 2, 2, 3, 0, 4, 2 };
        int val2 = 2;

        System.out.println("Case 1 : ");
        RemoveEle(nums1, val1);
        System.out.println();

        System.out.println("Case 2 : ");
        RemoveEle(nums2, val2);

    }

    public static void RemoveEle(int[] nums, int val) {
        System.out.print("[");
        for (int i = 0; i < nums.length; i++) {
            System.out.print(nums[i]);
            if ((i + 1) != nums.length) {
                System.out.print(",");
            }

        }
        System.out.println("]");

        String[] s_nums = new String[nums.length];
        int knums = 0;
        for (int i = 0; i < nums.length; i++) {
            s_nums[i] = "_";
        }

        for (int i = 0, j = 0; i < nums.length; i++) {
            if (nums[i] == val) {
                continue;
            }
            s_nums[j] = String.valueOf(nums[i]);
            j++;
            knums++;
        }
        System.out.println("k = " + knums);
        System.out.print("[");
        for (int i = 0; i < s_nums.length; i++) {
            System.out.print(s_nums[i]);
            if ((i + 1) != nums.length) {
                System.out.print(",");
            }

        }
        System.out.println("]");

    }
}
